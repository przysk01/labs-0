﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    public class Program
    {
        public static void Main(string[] args)
        {
            do
            {
                Console.WriteLine("Wybierz aparat:");
                Console.WriteLine("1. Aparat kompaktowy");
                Console.WriteLine("2. Lustrzanka cyfrowa");
                Console.WriteLine("Wpisz cyfrę:");
                string wybor = Convert.ToString(Console.ReadLine());
                aparaty_fotograficzne aparat = new aparaty_fotograficzne();
                Console.WriteLine();
                if (wybor == "1")
                {
                    Console.WriteLine("Wybrałeś kompakt");
                    aparat = new kompakty();
                    int obiektyw = aparat.wysun_obiektyw();
                    Console.WriteLine("Kompakt ma obiektyw o długości " + obiektyw + "mm");
                    Console.ReadKey();
                }
                else if (wybor == "2")
                {
                    Console.WriteLine("Wybrałeś lustrzankę");
                    aparat = new lustrzanki();
                    int obiektyw = aparat.wysun_obiektyw();
                    Console.WriteLine("Lustrzanka ma obiektyw o długości " + obiektyw + "mm");
                    Console.ReadKey();
                }
                else
                {
                    aparat = new aparaty_fotograficzne();
                    int obiektyw = aparat.wysun_obiektyw();
                    Console.WriteLine("Wybrałeś ukryty obiektyw o długości:" + obiektyw + "mm");
                }
                Console.WriteLine();
            } while (true);

        }
    }
}
